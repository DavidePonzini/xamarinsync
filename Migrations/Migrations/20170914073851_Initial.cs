﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Migrations.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Aree",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CodArea = table.Column<string>(nullable: true),
                    Descrizione = table.Column<string>(nullable: true),
                    IdMemoNote = table.Column<int>(nullable: false),
                    State = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aree", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "Teste",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AnnoCommerciale = table.Column<bool>(nullable: false),
                    ChiusuraAutomaticaScadenze = table.Column<bool>(nullable: false),
                    CodContoIncassi = table.Column<string>(nullable: true),
                    CodPagamento = table.Column<int>(nullable: false),
                    Descrizione = table.Column<string>(nullable: true),
                    NrRate = table.Column<int>(nullable: false),
                    PagamentoAnticipato = table.Column<bool>(nullable: false),
                    PercMaggiorazione = table.Column<float>(nullable: false),
                    PercSconto = table.Column<float>(nullable: false),
                    RateCostanti = table.Column<bool>(nullable: false),
                    SpeseIncasso = table.Column<float>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    TipoGestioneIva = table.Column<int>(nullable: false),
                    TipoPagamentoPA = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teste", x => x.Guid);
                    table.UniqueConstraint("AK_Teste_CodPagamento", x => x.CodPagamento);
                });

            migrationBuilder.CreateTable(
                name: "Operai",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AttivitaPerAltri = table.Column<bool>(nullable: false),
                    AttivitaTicketPerAltri = table.Column<bool>(nullable: false),
                    BloccaGruppo = table.Column<bool>(nullable: false),
                    BloccaOperatore = table.Column<bool>(nullable: false),
                    Cellulare = table.Column<string>(nullable: true),
                    CodCentro = table.Column<string>(nullable: true),
                    CodContoANAManodoperaDefault = table.Column<string>(nullable: true),
                    CodContoANAManodperaDefaultCdc = table.Column<string>(nullable: true),
                    CodOperaio = table.Column<string>(nullable: true),
                    CodRuolo = table.Column<string>(nullable: true),
                    CostNotturnoFestivo = table.Column<float>(nullable: false),
                    CostoFestivo = table.Column<float>(nullable: false),
                    CostoMan = table.Column<float>(nullable: false),
                    CostoNotturno = table.Column<float>(nullable: false),
                    CostoStraordinario = table.Column<float>(nullable: false),
                    DepositoDefault = table.Column<string>(nullable: true),
                    Descrizione = table.Column<string>(nullable: true),
                    Disattivo = table.Column<bool>(nullable: false),
                    GruppoCrm = table.Column<string>(nullable: true),
                    IdCalendario = table.Column<int>(nullable: false),
                    ImpegniAttivi = table.Column<bool>(nullable: false),
                    Interno = table.Column<int>(nullable: false),
                    Mail = table.Column<string>(nullable: true),
                    OperatoreCalendario = table.Column<bool>(nullable: false),
                    OperatoreTSQL = table.Column<bool>(nullable: false),
                    OutlookAttivo = table.Column<bool>(nullable: false),
                    Predefinito = table.Column<bool>(nullable: false),
                    RicavoMan = table.Column<float>(nullable: false),
                    ScheduleID = table.Column<int>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    TempoMan = table.Column<int>(nullable: false),
                    UtenteLogin = table.Column<string>(nullable: true),
                    codEtichetta = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operai", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "LastSynchronizations",
                columns: table => new
                {
                    Feed = table.Column<string>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LastSynchronizations", x => x.Feed);
                });

            migrationBuilder.CreateTable(
                name: "Righe",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CodPagamento = table.Column<int>(nullable: false),
                    FineMese = table.Column<bool>(nullable: false),
                    GgRata = table.Column<int>(nullable: false),
                    IdTipologiaPagamento = table.Column<int>(nullable: false),
                    NrRata = table.Column<int>(nullable: false),
                    PercRata = table.Column<float>(nullable: false),
                    State = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Righe", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_Righe_Teste_CodPagamento",
                        column: x => x.CodPagamento,
                        principalTable: "Teste",
                        principalColumn: "CodPagamento",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Righe_CodPagamento",
                table: "Righe",
                column: "CodPagamento");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Aree");

            migrationBuilder.DropTable(
                name: "Righe");

            migrationBuilder.DropTable(
                name: "Operai");

            migrationBuilder.DropTable(
                name: "LastSynchronizations");

            migrationBuilder.DropTable(
                name: "Teste");
        }
    }
}
