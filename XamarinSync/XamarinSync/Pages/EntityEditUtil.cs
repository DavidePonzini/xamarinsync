﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XamarinSync.Database.Entities;
using EntityState = XamarinSync.Database.EntityState;

namespace XamarinSync.Pages
{
	public static class EntityEditUtil
	{
		public static void Init<TEntity>(TEntity entity, out TEntity outEntity, out bool bIsNewEntity)
			where TEntity : Entity, new() 
		{
			if (entity == null)
			{
				outEntity = new TEntity();
				bIsNewEntity = true;
			}
			else
			{
				outEntity = entity;
				bIsNewEntity = false;
			}
		}

		public static async Task SaveEdit<TEntity>(TEntity entity)
			where TEntity : Entity
		{
			var context = await App.GetDatabaseContext();

			entity.State = EntityState.Modified;
			
			await context.SaveChangesAsync();
		}

		public static async Task SaveInsert<TEntity>(TEntity entity, DbSet<TEntity> dbSet)
			where TEntity : Entity
		{
			var context = await App.GetDatabaseContext();

			entity.State = EntityState.New;
			await dbSet.AddAsync(entity);
			
			await context.SaveChangesAsync();
		}
	}
}
