﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityAreeEditPage : ContentPage
	{
		private readonly Area _area;
		private readonly bool _bIsNewEntity;

		public EntityAreeEditPage(Area area = null)
		{
			InitializeComponent();

			EntityEditUtil.Init(area, out _area, out _bIsNewEntity);

			EntryDescrizione.Text = _area.Descrizione;
		}


		private async void ButtonSave_OnClicked(object sender, EventArgs e)
		{
			_area.Descrizione = EntryDescrizione.Text;

			if (_bIsNewEntity)
			{
				var context = await App.GetDatabaseContext();

				await EntityEditUtil.SaveInsert(_area, context.Aree);
			}
			else
			{
				await EntityEditUtil.SaveEdit(_area);
			}

			await Navigation.PopAsync();
		}
	}
}