﻿namespace XamarinSync.Database.Entities
{
	public class Operai : Entity
	{
		public string CodOperaio { get; set; }
		public string Descrizione { get; set; }
		public string CodCentro { get; set; }
		public int TempoMan { get; set; }
		public float CostoMan { get; set; }
		public float CostoStraordinario { get; set; }
		public float CostoFestivo { get; set; }
		public float CostoNotturno { get; set; }
		public float CostNotturnoFestivo { get; set; }
		public string CodRuolo { get; set; }
		public int Interno { get; set; }
		public string CodContoANAManodoperaDefault { get; set; }
		public string Mail { get; set; }
		public string Cellulare { get; set; }
		public bool Disattivo { get; set; }
		public string UtenteLogin { get; set; }
		public bool AttivitaPerAltri { get; set; }
		public bool Predefinito { get; set; }
		public bool ImpegniAttivi { get; set; }
		public string DepositoDefault { get; set; }
		public bool OperatoreCalendario { get; set; }
		public int ScheduleID { get; set; }
		public string codEtichetta { get; set; }
		public bool OutlookAttivo { get; set; }
		public float RicavoMan { get; set; }
		public bool BloccaGruppo { get; set; }
		public bool BloccaOperatore { get; set; }
		public bool OperatoreTSQL { get; set; }
		public string GruppoCrm { get; set; }
		public string CodContoANAManodperaDefaultCdc { get; set; }
		public int IdCalendario { get; set; }
		public bool AttivitaTicketPerAltri { get; set; }
		public override object[] Keys => new object[] {CodOperaio};

		public override string ToString()
		{
			return CodOperaio + " " + Descrizione;
		}
	}
}
