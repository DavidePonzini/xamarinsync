﻿using System;

namespace XamarinSync.Synchronization.Conflicts
{
	public class IgnoreEntityDifferenceAttribute : Attribute
	{ }
}
