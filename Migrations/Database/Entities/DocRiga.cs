﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinSync.Database.Entities
{
	public class DocRiga : Entity
	{
		public int IdDoc { get; set; }

		public int IdRiga { get; set; }
		public float Qta { get; set; }

		public virtual Articolo Articolo { get; set; }
		public string CodArt { get; set; }

		public override object[] Keys => new object[] {IdDoc, IdRiga};
	}
}
