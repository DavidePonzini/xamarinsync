﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;

namespace XamarinSync.Database.Entities
{
	public abstract class Entity
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Guid { get; set; }

		[JsonIgnore]
		public abstract object[] Keys { get; }

		public EntityState State { get; set; } = EntityState.Unchanged;

		internal void SetKeys(EntityKeys[] keys)
		{
			var type = GetType();

			foreach (var key in keys)
				type.GetProperty(key.Name).SetValue(this, key.Value);
		}

		public override string ToString()
		{
			return Keys.Aggregate("", (current, key) => current + (key + " "));
		}
	}
}
