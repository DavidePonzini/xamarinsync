﻿	using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectPageDetail : ContentPage
	{
		private readonly Type _itemType;
		private IEnumerable<Entity> _items;

		public SelectPageDetail()
		{
			Content = new StackLayout
			{
				Children =
				{
					new Image
					{
						Source = "edisoftware.png"
					},
					new Label
					{
						Text = "Seleziona un'entità",
						FontSize = 24,
						HorizontalOptions = LayoutOptions.Center,
						TextColor = Color.RoyalBlue
					},
					new Label
					{
						Text = "dal menù a sinistra",
						FontSize = 24,
						HorizontalOptions = LayoutOptions.Center,
						TextColor = Color.RoyalBlue
					}
				}
			};
		}

		public SelectPageDetail(IEnumerable<Entity> items, Type itemType)
		{
			InitializeComponent();

			_itemType = itemType;
			_items = items;
			ListView.ItemsSource = _items;
		}

		
		private async void List_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			Page page;

			if (e.SelectedItem is Area area)
				page = new EntityAreeVisualizePage(area);

			else if (e.SelectedItem is CondizioniPagamentoRighe riga)
				page = new EntityRigheVisualizePage(riga);

			else if (e.SelectedItem is CondizioniPagamentoTeste testa)
				page = new EntityTesteVisualizePage(testa);

			else if (e.SelectedItem is Operai operaio)
				page = new EntityOperaiVisualizePage(operaio);

			else if (e.SelectedItem is DocTesta docTesta)
				page = new EntityDocTestaVisualizePage(docTesta);

			else
				throw new ArgumentOutOfRangeException();

			await Navigation.PushAsync(page);
		}

		private async void ButtonInsert_OnClicked(object sender, EventArgs e)
		{
			Page page;

			if (_itemType == typeof(Area))
				page = new EntityAreeEditPage();

			else if (_itemType == typeof(CondizioniPagamentoRighe))
				page = new EntityRigheEditPage();

			else if (_itemType == typeof(CondizioniPagamentoTeste))
				page = new EntityTesteEditPage();

			else if (_itemType == typeof(Operai))
				page = new EntityOperaiEditPage();

			else if (_itemType == typeof(DocTesta))
				page = new EntityDocTestaEditPage();

			else
				throw new ArgumentOutOfRangeException();

			await Navigation.PushAsync(page);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (ListView != null)
				ListView.ItemsSource = _items;
		}
	}
}