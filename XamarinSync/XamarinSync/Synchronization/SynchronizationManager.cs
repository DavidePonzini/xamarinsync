﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XamarinSync.Database;
using XamarinSync.Database.Entities;
using XamarinSync.Synchronization.Conflicts;
using XamarinSync.Synchronization.Connection;
using EntityState = XamarinSync.Database.EntityState;

namespace XamarinSync.Synchronization
{
	public static class SynchronizationManager
	{
		private const int PageSize = 10000;

		public static async Task Synchronize()
		{
			var context = await App.GetDatabaseContext();

			await DownloadAsync("Aree", context.Aree);
			await DownloadAsync("CondizioniPagamentoTeste", context.Teste);
			await DownloadAsync("CondizioniPagamentoRighe", context.Righe);
			await DownloadAsync("operai", context.Operai);
			await DownloadAsync("Articoli", context.Articoli);
			await DownloadAsync("Clienti", context.Clienti);
			await DownloadAsync("OrdiniTesta", context.DocTeste);
			await DownloadAsync("OrdiniRighe", context.DocRighe);
			await DownloadAsync("OrdiniCoda", context.DocCode);
			
			if (ConflictManager.Conflicts.Count > 0)
				throw new UnresolvedConflictsException();
	
			//await UploadAsync("Aree", context.Aree);
			//await UploadAsync("CondizioniPagamentoTeste", context.Teste);
			//await UploadAsync("operai", context.Operai);
		}

		private static async Task<TEntity> GetLocalAsync<TEntity>(IEnumerable<object> keys, DbSet<TEntity> dbSet)
			where TEntity : Entity
		{
			var res = await dbSet.SingleOrDefaultAsync(entity => entity.Keys.SequenceEqual(keys));
			return res;
		}

		private static async Task DownloadAsync<TEntity>(string feed, DbSet<TEntity> dbSet)
			where TEntity : Entity, new()
		{
			var context = await App.GetDatabaseContext();

			var lastSync = await context.LastSynchronizations.SingleOrDefaultAsync(entity => entity.Feed == feed);

			var currentPage = 0;

			var bFirstInit = !dbSet.Any();

			while (true)
			{
				OdataResponse<TEntity> odata;
				if (lastSync != null)
					odata = await ConnectionManager.DownloadAsync<TEntity>(feed, lastSync.Time, PageSize, currentPage * PageSize);
				else
					odata = await ConnectionManager.DownloadAsync<TEntity>(feed, PageSize, currentPage * PageSize);

				foreach (var downloadedEntity in odata.Values)
				{
					if (bFirstInit)
						await dbSet.AddAsync(downloadedEntity);
					else
					{
						var localEntity = await GetLocalAsync(downloadedEntity.Keys, dbSet);

						if (localEntity == null)
							await dbSet.AddAsync(downloadedEntity);
						else
							await ConflictManager.UpdateAsync(localEntity, downloadedEntity);
					}
				}

				await context.SaveChangesAsync();

				currentPage++;

				// less data than page size means this was the last page
				if (odata.Values.Length < PageSize)
					break;
			}
			
			if (lastSync == null)
			{
				var lastSyncEntity = new LastSynchronization
				{
					Feed = feed,
					Time = DateTime.Now
				};

				await context.LastSynchronizations.AddAsync(lastSyncEntity);
			}
			else
			{
				lastSync.Time = DateTime.Now;
			}
		}

		public static async Task UploadAsync<TEntity>(string feed, DbSet<TEntity> dbSet)
			where TEntity : Entity, new()
		{
			foreach(var entity in dbSet)
			{
				switch (entity.State)
				{
				case EntityState.Unchanged:
					continue;
				case EntityState.Modified:
					await ConnectionManager.UpdateAsync(feed, entity);
					entity.State = EntityState.Unchanged;
					break;
				case EntityState.New:
					var keys = await ConnectionManager.InsertAsync(feed, entity);

					entity.SetKeys(keys);

					entity.State = EntityState.Unchanged;
					break;
				default:
					throw new ArgumentOutOfRangeException();
				}
			}

			var context = await App.GetDatabaseContext();
			await context.SaveChangesAsync();
		}
	}
}
