﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace XamarinSync.Database.Entities
{
	public class CondizioniPagamentoRighe : Entity
	{
		public int CodPagamento { get; set; } 
		public int NrRata { get; set; }
		public int GgRata { get; set; }
		public float PercRata { get; set; }
		public bool FineMese { get; set; } = false;
		public int IdTipologiaPagamento { get; set; }
		
		[JsonIgnore]
		public virtual CondizioniPagamentoTeste Testa { get; set; }

		[NotMapped]
		public override object[] Keys => new object[] {CodPagamento, NrRata};

		public override string ToString()
		{
			return Testa.Descrizione + " " + CodPagamento;
		}
	}
}
