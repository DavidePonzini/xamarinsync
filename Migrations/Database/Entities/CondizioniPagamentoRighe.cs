﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace XamarinSync.Database.Entities
{
	public class CondizioniPagamentoRighe : Entity
	{
		public int CodPagamento { get; set; } = 0;
		public int NrRata { get; set; } = 0;
		public int GgRata { get; set; } = 0;
		public float PercRata { get; set; } = 0f;
		public bool FineMese { get; set; } = false;
		public int IdTipologiaPagamento { get; set; } = 0;
		
		[JsonIgnore]
		public virtual CondizioniPagamentoTeste Testa { get; set; }

		[NotMapped]
		public override object[] Keys => new object[] {CodPagamento, NrRata};

		public override string ToString()
		{
			return Testa.Descrizione + " " + CodPagamento;
		}
	}
}
