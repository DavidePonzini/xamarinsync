﻿namespace XamarinSync.Synchronization.Conflicts
{
	public enum ConflictSolution
	{
		KeepLocal,
		KeepDownloaded,
		Abort
	}
}
