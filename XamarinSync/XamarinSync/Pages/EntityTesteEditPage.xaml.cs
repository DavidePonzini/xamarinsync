﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityTesteEditPage : ContentPage
	{
		private readonly CondizioniPagamentoTeste _testa;
		private readonly bool _bIsNewEntity;

		public EntityTesteEditPage(CondizioniPagamentoTeste testa = null)
		{
			InitializeComponent();

			EntityEditUtil.Init(testa, out _testa, out _bIsNewEntity);

			LabelCodice.Text = _testa.CodPagamento.ToString();
			EntryDescrizione.Text = _testa.Descrizione;
			EntryCodConto.Text = _testa.CodContoIncassi;
			EntryNumeroRate.Text = _testa.NrRate.ToString();
			EntryPercMagg.Text = _testa.PercMaggiorazione.ToString("F");
			EntryPercSconto.Text = _testa.PercSconto.ToString("F");
			EntrySpese.Text = _testa.SpeseIncasso.ToString("F");
			EntryGestioneIva.Text = _testa.TipoGestioneIva.ToString();
			EntryTipoPagamento.Text = _testa.TipoPagamentoPA;

		}


		private async void ButtonSave_OnClicked(object sender, EventArgs e)
		{
			_testa.Descrizione = EntryDescrizione.Text;
			_testa.CodContoIncassi = EntryCodConto.Text;
			_testa.NrRate = int.Parse(EntryNumeroRate.Text);
			_testa.PercMaggiorazione = float.Parse(EntryPercMagg.Text);
			_testa.PercSconto = float.Parse(EntryPercSconto.Text);
			_testa.SpeseIncasso = float.Parse(EntrySpese.Text);
			_testa.TipoGestioneIva = int.Parse(EntryGestioneIva.Text);
			_testa.TipoPagamentoPA = EntryTipoPagamento.Text;

			if (_bIsNewEntity)
			{
				var context = await App.GetDatabaseContext();

				await EntityEditUtil.SaveInsert(_testa, context.Teste);
			}
			else
			{
				await EntityEditUtil.SaveEdit(_testa);
			}

			await Navigation.PopAsync();
		}


	}
}