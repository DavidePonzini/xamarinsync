﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using XamarinSync.Database.Entities;

namespace XamarinSync.Synchronization.Conflicts
{
	public class EntityDifference
	{
		public string Name => _propertyInfo.Name;
		public object LocalValue { get; }
		public object DownloadedValue { get; }

		public event EventHandler OnSolved;

		private readonly PropertyInfo _propertyInfo;
		private readonly Entity _entity;

		public EntityDifference(object localValue, object downloadedValue, PropertyInfo propertyInfo, Entity entity)
		{
			LocalValue = localValue;
			DownloadedValue = downloadedValue;
			_propertyInfo = propertyInfo;
			_entity = entity;
		}

		public async Task<bool> Solve(ConflictSolution solution)
		{
			switch (solution)
			{
			case ConflictSolution.KeepLocal:
				break;

			case ConflictSolution.KeepDownloaded:
				_propertyInfo.SetValue(_entity, DownloadedValue);

				var context = await App.GetDatabaseContext();
				await context.SaveChangesAsync();

				break;

			default:
				return false;
			}

			OnSolved?.Invoke(this, EventArgs.Empty);

			return true;
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
