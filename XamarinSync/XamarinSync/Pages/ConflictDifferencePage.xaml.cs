﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Synchronization.Conflicts;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConflictDifferencePage : ContentPage
	{
		private readonly Conflict _conflict;
		private readonly ObservableCollection<EntityDifference> _differences;

		public ConflictDifferencePage(Conflict conflict)
		{
			InitializeComponent();

			_conflict = conflict;

			Title = _conflict.ToString();

			_differences = new ObservableCollection<EntityDifference>(conflict.Differences);
			ListView.ItemsSource = _differences;
		}

		private async void List_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var diff = (EntityDifference) e.SelectedItem;

			var bOverwrite = await DisplayAlert(diff.Name, "Local: " + diff.LocalValue + "\nNew: " + diff.DownloadedValue, "Keep new", "Keep local");

			if(await diff.Solve(bOverwrite ? ConflictSolution.KeepDownloaded : ConflictSolution.KeepLocal))
				_differences.Remove(diff);

			if (_conflict.Differences.Count == 0)
				await Navigation.PopAsync();
		}
	}
}