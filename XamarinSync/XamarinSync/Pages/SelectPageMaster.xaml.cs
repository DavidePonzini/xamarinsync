﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectPageMaster : ContentPage
	{
		public ListView ListView { get; }

		public SelectPageMaster()
		{
			InitializeComponent();

			BindingContext = new SelectPageMasterViewModel();
			ListView = MenuItemsListView;
		}

		public class SelectPageMasterViewModel : INotifyPropertyChanged
		{
			public ObservableCollection<SelectPageMenuItem> MenuItems { get; set; }

			public SelectPageMasterViewModel()
			{
				var context = App.GetDatabaseContext().Result;

				MenuItems = new ObservableCollection<SelectPageMenuItem>(new[]
				{
					new SelectPageMenuItem
					{
						Id = 0, Title = "Aree",
						Entities = context.Aree, EntityType = typeof(Area)
					},
					new SelectPageMenuItem
					{
						Id = 1, Title = "Condizioni Pagamento Righe",
						Entities = context.Righe, EntityType = typeof(CondizioniPagamentoRighe)
					},
					new SelectPageMenuItem
					{
						Id = 2, Title = "Condizioni Pagamento Teste",
						Entities = context.Teste, EntityType = typeof(CondizioniPagamentoTeste)
					},
					new SelectPageMenuItem
					{
						Id = 3, Title = "Operai",
						Entities = context.Operai, EntityType = typeof(Operai)
					},
					new SelectPageMenuItem
					{
						Id = 4, Title = "Ordini",
						Entities = context.DocTeste, EntityType = typeof(DocTesta)
					}

				});
			}

			#region INotifyPropertyChanged Implementation
			public event PropertyChangedEventHandler PropertyChanged;
			void OnPropertyChanged([CallerMemberName] string propertyName = "")
			{
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
			#endregion
		}
	}
}