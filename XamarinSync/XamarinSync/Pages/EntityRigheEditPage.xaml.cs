﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityRigheEditPage : ContentPage
	{
		private CondizioniPagamentoRighe _riga;
		private readonly bool _bIsNewEntity;

		public EntityRigheEditPage(CondizioniPagamentoRighe riga = null)
		{
			InitializeComponent();

			EntityEditUtil.Init(riga, out _riga, out _bIsNewEntity);

			LabelCodPagamento.Text = _riga.CodPagamento + "-" + _riga.Testa.Descrizione;
			EntryGiorniRata.Text = _riga.GgRata.ToString();
			EntryNumeroRata.Text =_riga.NrRata.ToString();
			EntryTipologiaPagamento.Text = _riga.IdTipologiaPagamento.ToString();
			EntryPercentualeRata.Text = _riga.PercRata.ToString("F");
		}

		private async void ButtonSave_OnClicked(object sender, EventArgs e)
		{
			_riga.GgRata = int.Parse(EntryGiorniRata.Text);
			_riga.NrRata = int.Parse(EntryNumeroRata.Text);
			_riga.PercRata = float.Parse(EntryPercentualeRata.Text);
			_riga.IdTipologiaPagamento = int.Parse(EntryTipologiaPagamento.Text);

			if (_bIsNewEntity)
			{
				var context = await App.GetDatabaseContext();

				await EntityEditUtil.SaveInsert(_riga, context.Righe);
			}
			else
			{
				await EntityEditUtil.SaveEdit(_riga);
			}

			await Navigation.PopAsync();
		}
	}
}