﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;
using XamarinSync.Database;
using XamarinSync.Pages;

namespace XamarinSync
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
			MainPage = new MainPage();
		}


		private static Context _context;
		public static async Task<Context> GetDatabaseContext()
		{
			if (_context == null)
			{
				var fileHelper = DependencyService.Get<IFileHelper>();
				var path = fileHelper.GetFilePath("db.me");
				_context = new Context(path);

				await _context.Database.EnsureDeletedAsync();

				await _context.Database.MigrateAsync();
			}

			return _context;
		}
	}
}
