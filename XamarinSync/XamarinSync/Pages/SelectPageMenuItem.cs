﻿using System;
using System.Collections.Generic;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{

	public class SelectPageMenuItem
	{
		public SelectPageMenuItem()
		{
			TargetType = typeof(SelectPageDetail);
		}
		public int Id { get; set; }
		public string Title { get; set; }
		public IEnumerable<Entity> Entities { get; set; }
		public Type EntityType { get; set; }

		public Type TargetType { get; set; }
	}
}