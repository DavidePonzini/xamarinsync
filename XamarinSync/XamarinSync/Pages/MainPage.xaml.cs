﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Synchronization.Connection;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
		
		public MainPage ()
		{
			InitializeComponent ();
		}

		private async void Button_OnClicked(object sender, EventArgs e)
		{
			try
			{
				await ConnectionManager.LoginAsync(UUsername.Text, UPassword.Text);
			}
			catch (ConnectionFailedException)
			{
				// TODO
				throw;
			}

			await  Navigation.PushModalAsync(new SelectPage());
		}

		private void ButtonTEST_OnClicked(object sender, EventArgs e)
		{
			ConnectionManager.TEST();
		}
	}
}