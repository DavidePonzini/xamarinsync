﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityTesteVisualizePage : ContentPage
	{
		private readonly CondizioniPagamentoTeste _testa;

		public EntityTesteVisualizePage(CondizioniPagamentoTeste testa)
		{
			InitializeComponent();

			_testa = testa;

			InitValues();
		}

		private void InitValues()
		{
			LabelDescrizione.Text = _testa.Descrizione;
			LabelCodice.Text = _testa.CodPagamento.ToString();

			LabelNumeroRate.Text = _testa.NrRate.ToString();
			LabelPercMagg.Text = _testa.PercMaggiorazione.ToString("P");
			LabelPercSconto.Text = _testa.PercSconto.ToString("P");

			LabelSpese.Text = _testa.SpeseIncasso.ToString("C");
			LabelGestioneIva.Text = _testa.TipoGestioneIva.ToString();

			LabelTipoPagamento.Text = _testa.TipoPagamentoPA;
			LabelCodConto.Text = _testa.CodPagamento.ToString();

			ListViewRighe.ItemsSource = _testa.Righe;
		}

		private async void ButtonEdit_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new EntityTesteEditPage(_testa));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			InitValues();
		}
	}
}