﻿namespace XamarinSync.Database.Entities
{
	public class Cliente : Entity
	{
		public int IdAnagrafica { get; set; }
		public string RagioneSociale { get; set; }

		public override object[] Keys => new object[] {IdAnagrafica};
	}
}