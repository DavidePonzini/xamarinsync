﻿using System;

namespace XamarinSync.Synchronization.Connection
{
	[Serializable]
	internal class LoginRequiredException : Exception
	{
		public LoginRequiredException()
		{
		}

		public LoginRequiredException(string message) : base(message)
		{
		}

		public LoginRequiredException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}