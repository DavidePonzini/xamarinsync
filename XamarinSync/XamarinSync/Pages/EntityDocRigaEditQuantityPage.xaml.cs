﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityDocRigaEditQuantityPage : ContentPage
	{
		private readonly DocRiga _riga;

		public EntityDocRigaEditQuantityPage(DocRiga riga)
		{
			InitializeComponent();

			_riga = riga;
			
			Entry.Keyboard = Keyboard.Numeric;
			Entry.Text = _riga.Qta.ToString("F");
		}

		public event EventHandler OnSave;

		private async void ButtonConfirm_OnClicked(object sender, EventArgs e)
		{
			if (!float.TryParse(Entry.Text, out float value))
				value = 0f;

			_riga.Qta = value;

			OnSave?.Invoke(_riga, EventArgs.Empty);
			await Navigation.PopModalAsync();
		}
	}
}