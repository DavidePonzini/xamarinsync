﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityDocTestaVisualizePage : ContentPage
	{
		private readonly DocTesta _testa;
		public EntityDocTestaVisualizePage(DocTesta testa)
		{
			InitializeComponent();

			_testa = testa;
			InitValues();
		}

		private void InitValues()
		{
			LabelCliente.Text = _testa.Cliente.RagioneSociale;
			LabelCodaPercSconto.Text = _testa.Coda.PercSconto.ToString("P");
			LabelDataDoc.Text = _testa.DataDocumento.ToString("g");
			LabelTotDoc.Text = _testa.TotDocumento.ToString("C");
			LabelNumeroDoc.Text = _testa.NumeroDocumento.ToString();
			ListViewRighe.ItemsSource = _testa.Righe;
		}

		private async void ButtonEdit_OnClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new EntityDocTestaEditPage(_testa));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			InitValues();
		}
	}
}