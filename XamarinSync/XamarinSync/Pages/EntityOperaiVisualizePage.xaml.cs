﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityOperaiVisualizePage : ContentPage
	{
		private readonly Operai _operaio;

		public EntityOperaiVisualizePage(Operai operaio)
		{
			InitializeComponent();

			_operaio = operaio;
			InitValue();
		}

		private void InitValue()
		{
			LabelDescrizione.Text = _operaio.Descrizione;
			LabelCellulare.Text = _operaio.Cellulare;
			LabelCodCentro.Text = _operaio.CodCentro;
			LabelCodOp.Text = _operaio.CodOperaio;
			LabelCostoMan.Text = _operaio.CostoMan.ToString("C");
			LabelMail.Text = _operaio.Mail;
		}

		private async void ButtonEdit_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new EntityOperaiEditPage(_operaio));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			InitValue();
		}
	}
}