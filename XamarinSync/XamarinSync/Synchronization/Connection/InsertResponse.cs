﻿using Newtonsoft.Json;
using XamarinSync.Database;

namespace XamarinSync.Synchronization.Connection
{
	internal class InsertResponse
	{
		[JsonProperty("Data")]
		public EntityKeys[] Keys = {};
	}
}