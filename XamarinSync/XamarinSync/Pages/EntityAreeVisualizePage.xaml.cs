﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityAreeVisualizePage : ContentPage
	{
		private readonly Area _area;

		public EntityAreeVisualizePage(Area area)
		{
			InitializeComponent();

			_area = area;

			LabelDescrizione.Text = _area.Descrizione;
		}

		private async void ButtonEdit_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new EntityAreeEditPage(_area));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			LabelDescrizione.Text = _area.Descrizione;
		}
	}
}