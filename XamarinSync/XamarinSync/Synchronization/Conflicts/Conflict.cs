﻿using System;
using System.Collections.Generic;
using System.Reflection;
using XamarinSync.Database.Entities;

namespace XamarinSync.Synchronization.Conflicts
{
	public class Conflict
	{
		public Entity Local { get; set; }
		public Entity Downloaded { get; set; }

		public event EventHandler ConflictSolved;

		public Conflict(Entity local, Entity downloaded)
		{
			Local = local;
			Downloaded = downloaded;

			Differences = new List<EntityDifference>();

			foreach (var propertyInfo in Local.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
			{
				if (propertyInfo.GetCustomAttribute<IgnoreEntityDifferenceAttribute>() != null)
					continue;

				var localValue = propertyInfo.GetValue(Local);
				var downloadedValue = propertyInfo.GetValue(Downloaded);

				if (Equals(localValue, downloadedValue))
					continue;

				var entityDifference = new EntityDifference(localValue, downloadedValue, propertyInfo, Local);
				entityDifference.OnSolved += OnDifferenceSolved;

				Differences.Add(entityDifference);
			}
		}

		private void OnDifferenceSolved(object sender, EventArgs e)
		{
			var entityDifference = (EntityDifference) sender;

			Differences.Remove(entityDifference);

			if (Differences.Count == 0)
				ConflictSolved?.Invoke(this, EventArgs.Empty);
		}

		public List<EntityDifference> Differences { get; }

		public override string ToString()
		{
			return Local.ToString();
		}
	}
}