﻿using System.ComponentModel.DataAnnotations.Schema;

namespace XamarinSync.Database.Entities
{
	public class Area : Entity
	{
		public string CodArea { get; set; }

		public string Descrizione { get; set; }
		public int IdMemoNote { get; set; } = 0;
		
		[NotMapped]
		public override object[] Keys => new object[] {CodArea};

		[NotMapped]
		public int EntityState { get; set; } = 1;

		public override string ToString()
		{
			return CodArea;
		}
	}
}
