﻿using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Synchronization.Conflicts;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConflictPage : ContentPage
	{
		public ConflictPage()
		{
			InitializeComponent();
		}

		private async void ConflictListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if(e.SelectedItem == null)
				return;

			var conflict = (Conflict) e.SelectedItem;

			await Navigation.PushAsync(new ConflictDifferencePage(conflict));

			ListView.SelectedItem = null;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			ListView.ItemsSource = ConflictManager.Conflicts;

			if (!ConflictManager.Conflicts.Any())
			{
				var closeButton = new Button {Text = "Close"};
				closeButton.Clicked += async (sender, args) => await Navigation.PopModalAsync();

				Content = new StackLayout
				{
					Children =
					{
						new Label
						{
							Text = "All conflicts have been solved",
							FontSize = 24,
							HorizontalOptions = LayoutOptions.Center
						}
						, closeButton
					}
				};
			}
		}
	}
}