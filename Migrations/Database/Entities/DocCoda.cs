﻿namespace XamarinSync.Database.Entities
{
	public class DocCoda : Entity
	{
		public int IdDoc { get; set; }

		public float PercSconto { get; set; }

		public override object[] Keys => new object[] {IdDoc};
	}
}