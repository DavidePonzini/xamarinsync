﻿using System;

namespace XamarinSync.Synchronization.Conflicts
{
	[Serializable]
	internal class UnresolvedConflictsException : Exception
	{
		public UnresolvedConflictsException()
		{
		}

		public UnresolvedConflictsException(string message) : base(message)
		{
		}

		public UnresolvedConflictsException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}