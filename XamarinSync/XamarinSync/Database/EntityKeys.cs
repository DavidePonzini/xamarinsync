using Newtonsoft.Json;

namespace XamarinSync.Database
{
	public class EntityKeys
	{
		[JsonProperty("Key")]
		public string Name;
		public object Value;
	}
}