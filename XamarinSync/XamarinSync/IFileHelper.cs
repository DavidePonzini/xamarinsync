﻿namespace XamarinSync
{
	public interface IFileHelper
	{
		string GetFilePath(string filename);
	}
}
