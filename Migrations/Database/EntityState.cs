﻿namespace XamarinSync.Database
{
	public enum EntityState
	{
		Unchanged,
		Modified,
		New
	}
}