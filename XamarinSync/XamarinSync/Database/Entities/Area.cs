﻿using System.ComponentModel.DataAnnotations.Schema;
using XamarinSync.Synchronization.Serialization;

namespace XamarinSync.Database.Entities
{
	public class Area : Entity
	{
		[JsonSerializeName("Area")]
		public string CodArea { get; set; }

		public string Descrizione { get; set; }
		public int IdMemoNote { get; set; } = 0;
		
		[NotMapped]
		public override object[] Keys => new object[] {CodArea};

		[NotMapped]
		public int EntityState { get; set; } = 1;

		public override string ToString()
		{
			return Descrizione;
		}
	}
}
