﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityRigheVisualizePage : ContentPage
	{
		private readonly CondizioniPagamentoRighe _riga;

		public EntityRigheVisualizePage(CondizioniPagamentoRighe riga)
		{
			InitializeComponent ();

			_riga = riga;

			InitValue();
		}

		private async void ButtonEdit_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new EntityRigheEditPage(_riga));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			InitValue();
		}

		private void InitValue()
		{
			LabelCodPagamento.Text = _riga.CodPagamento + "-" + _riga.Testa.Descrizione;
			LabelGiorniRata.Text = _riga.GgRata.ToString();
			LabelNumeroRata.Text = _riga.NrRata.ToString();
			LabelPercentualeRata.Text = _riga.PercRata.ToString("P");
			LabelTipologiaPagamento.Text = _riga.IdTipologiaPagamento.ToString();
		}
	}
}