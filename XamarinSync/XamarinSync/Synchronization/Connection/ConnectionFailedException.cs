﻿using System;
using System.Net.Http;

namespace XamarinSync.Synchronization.Connection
{
	[Serializable]
	internal class ConnectionFailedException : Exception
	{
		public HttpResponseMessage Response;

		public ConnectionFailedException()
		{
		}

		public ConnectionFailedException(HttpResponseMessage response)
		{
			this.Response = response;
		}

		public ConnectionFailedException(string message) : base(message)
		{
		}

		public ConnectionFailedException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}