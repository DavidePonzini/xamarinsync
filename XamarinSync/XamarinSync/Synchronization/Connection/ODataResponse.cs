﻿using Newtonsoft.Json;

namespace XamarinSync.Synchronization.Connection
{
	public class OdataResponse<T>
		where T : new()
	{
		// [JsonProperty("@odata.context")]
		// public string Context;

		[JsonProperty("value")]
		public T[] Values;
	}
}
