using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace XamarinSync.Synchronization.Serialization
{
	public class JsonContractResolver : DefaultContractResolver
	{
		protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
		{
			var properties = base.CreateProperties(type, memberSerialization);

			foreach (var property in properties)
			{
				var attribute = type.GetProperty(property.UnderlyingName)
					.GetCustomAttribute<JsonSerializeNameAttribute>();

				if(attribute == null)
					continue;

				var name = attribute.Name;

				property.PropertyName = name;
			}

			return properties;
		}
	}
}