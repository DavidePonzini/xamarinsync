﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XamarinSync.Database;
using XamarinSync.Database.Entities;

namespace XamarinSync.Synchronization.Conflicts
{
	public static class ConflictManager
	{
		public static List<Conflict> Conflicts { get; } = new List<Conflict>();

		public static async Task UpdateAsync<TEntity>(TEntity local, TEntity downloaded)
			where TEntity : Entity
		{
			if (local.State == EntityState.Modified)
			{
				var conflict = new Conflict(local, downloaded);
				if (conflict.Differences.Any())
				{
					conflict.ConflictSolved += (sender, args) => Conflicts.Remove((Conflict) sender);
					Conflicts.Add(conflict);
					return;
				}

				local.State = EntityState.Unchanged;
			}

			await OverwriteAsync(local, downloaded);
		}

		private static async Task OverwriteAsync<TEntity>(TEntity local, TEntity downloaded)
			where TEntity : Entity
		{
			downloaded.Guid = local.Guid;
			//downloaded.State = EntityState.Unchanged;

			var context = await App.GetDatabaseContext();
			context.Entry(local).CurrentValues.SetValues(downloaded);
		}
	}
}