﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntitySelectPage : ContentPage
	{
		public event EventHandler ItemSelected;

		public IEnumerable ItemSource
		{
			set => ListView.ItemsSource = value;
		}

		public EntitySelectPage()
		{
			InitializeComponent();
		}

		private async void ListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			ItemSelected?.Invoke(e.SelectedItem, EventArgs.Empty);
			await Navigation.PopAsync();
		}
	}
}