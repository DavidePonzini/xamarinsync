﻿using System;
using System.IO;
using Xamarin.Forms;
using XamarinSync.iOS;

[assembly: Dependency(typeof(FileHelper))]
namespace XamarinSync.iOS
{
	public class FileHelper : IFileHelper
	{
		public string GetFilePath(string filename)
		{
			var docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var libFolder = Path.Combine(docFolder, "..", "Library", "XamarinSync");

			if (!Directory.Exists(libFolder))
			{
				Directory.CreateDirectory(libFolder);
			}

			return Path.Combine(libFolder, filename);
		}
	}
}