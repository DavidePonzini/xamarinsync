﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace XamarinSync.Database.Entities
{
	public class CondizioniPagamentoTeste : Entity
	{
		public int CodPagamento { get; set; } = 0;
		public string Descrizione { get; set; } = "";
		public bool AnnoCommerciale { get; set; } = false;
		public int NrRate { get; set; } = 0;
		public float PercMaggiorazione { get; set; } = 0f;
		public float PercSconto { get; set; } = 0f;
		public float SpeseIncasso { get; set; } = 0f;
		public bool RateCostanti { get; set; } = false;
		public int TipoGestioneIva { get; set; } = 0;
		public string CodContoIncassi { get; set; } = "";
		public bool ChiusuraAutomaticaScadenze { get; set; } = false;
		public string TipoPagamentoPA { get; set; } = "";
		public bool PagamentoAnticipato { get; set; } = false;

		public virtual List<CondizioniPagamentoRighe> Righe { get; set; } = new List<CondizioniPagamentoRighe>();

		[NotMapped]
		public override object[] Keys => new object[] {CodPagamento};

		public override string ToString()
		{
			return Descrizione;
		}
	}
}
