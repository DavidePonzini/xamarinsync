﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using XamarinSync.Database;

namespace Migrations.Migrations
{
    [DbContext(typeof(Context))]
    partial class ContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("XamarinSync.Database.Entities.Area", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CodArea");

                    b.Property<string>("Descrizione");

                    b.Property<int>("IdMemoNote");

                    b.Property<int>("State");

                    b.HasKey("Guid");

                    b.ToTable("Aree");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.Articolo", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CodArt")
                        .IsRequired();

                    b.Property<string>("Descrizione");

                    b.Property<int>("State");

                    b.HasKey("Guid");

                    b.ToTable("Articoli");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.Cliente", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("IdAnagrafica");

                    b.Property<string>("RagioneSociale");

                    b.Property<int>("State");

                    b.HasKey("Guid");

                    b.ToTable("Clienti");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.CondizioniPagamentoRighe", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CodPagamento");

                    b.Property<bool>("FineMese");

                    b.Property<int>("GgRata");

                    b.Property<int>("IdTipologiaPagamento");

                    b.Property<int>("NrRata");

                    b.Property<float>("PercRata");

                    b.Property<int>("State");

                    b.HasKey("Guid");

                    b.HasIndex("CodPagamento");

                    b.ToTable("Righe");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.CondizioniPagamentoTeste", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AnnoCommerciale");

                    b.Property<bool>("ChiusuraAutomaticaScadenze");

                    b.Property<string>("CodContoIncassi");

                    b.Property<int>("CodPagamento");

                    b.Property<string>("Descrizione");

                    b.Property<int>("NrRate");

                    b.Property<bool>("PagamentoAnticipato");

                    b.Property<float>("PercMaggiorazione");

                    b.Property<float>("PercSconto");

                    b.Property<bool>("RateCostanti");

                    b.Property<float>("SpeseIncasso");

                    b.Property<int>("State");

                    b.Property<int>("TipoGestioneIva");

                    b.Property<string>("TipoPagamentoPA");

                    b.HasKey("Guid");

                    b.ToTable("Teste");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.DocCoda", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("IdDoc");

                    b.Property<float>("PercSconto");

                    b.Property<int>("State");

                    b.HasKey("Guid");

                    b.HasIndex("IdDoc")
                        .IsUnique();

                    b.ToTable("DocCode");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.DocRiga", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CodArt");

                    b.Property<int>("IdDoc");

                    b.Property<int>("IdRiga");

                    b.Property<float>("Qta");

                    b.Property<int>("State");

                    b.HasKey("Guid");

                    b.HasIndex("CodArt");

                    b.HasIndex("IdDoc");

                    b.ToTable("DocRighe");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.DocTesta", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DataDocumento");

                    b.Property<int>("IdAnagrafica");

                    b.Property<int>("IdDoc");

                    b.Property<int>("NumeroDocumento");

                    b.Property<int>("State");

                    b.Property<float>("TotDocumento");

                    b.HasKey("Guid");

                    b.HasIndex("IdAnagrafica");

                    b.ToTable("DocTeste");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.Operai", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AttivitaPerAltri");

                    b.Property<bool>("AttivitaTicketPerAltri");

                    b.Property<bool>("BloccaGruppo");

                    b.Property<bool>("BloccaOperatore");

                    b.Property<string>("Cellulare");

                    b.Property<string>("CodCentro");

                    b.Property<string>("CodContoANAManodoperaDefault");

                    b.Property<string>("CodContoANAManodperaDefaultCdc");

                    b.Property<string>("CodOperaio");

                    b.Property<string>("CodRuolo");

                    b.Property<float>("CostNotturnoFestivo");

                    b.Property<float>("CostoFestivo");

                    b.Property<float>("CostoMan");

                    b.Property<float>("CostoNotturno");

                    b.Property<float>("CostoStraordinario");

                    b.Property<string>("DepositoDefault");

                    b.Property<string>("Descrizione");

                    b.Property<bool>("Disattivo");

                    b.Property<string>("GruppoCrm");

                    b.Property<int>("IdCalendario");

                    b.Property<bool>("ImpegniAttivi");

                    b.Property<int>("Interno");

                    b.Property<string>("Mail");

                    b.Property<bool>("OperatoreCalendario");

                    b.Property<bool>("OperatoreTSQL");

                    b.Property<bool>("OutlookAttivo");

                    b.Property<bool>("Predefinito");

                    b.Property<float>("RicavoMan");

                    b.Property<int>("ScheduleID");

                    b.Property<int>("State");

                    b.Property<int>("TempoMan");

                    b.Property<string>("UtenteLogin");

                    b.Property<string>("codEtichetta");

                    b.HasKey("Guid");

                    b.ToTable("Operai");
                });

            modelBuilder.Entity("XamarinSync.Database.LastSynchronization", b =>
                {
                    b.Property<string>("Feed")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Time");

                    b.HasKey("Feed");

                    b.ToTable("LastSynchronizations");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.CondizioniPagamentoRighe", b =>
                {
                    b.HasOne("XamarinSync.Database.Entities.CondizioniPagamentoTeste", "Testa")
                        .WithMany("Righe")
                        .HasForeignKey("CodPagamento")
                        .HasPrincipalKey("CodPagamento")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.DocCoda", b =>
                {
                    b.HasOne("XamarinSync.Database.Entities.DocTesta")
                        .WithOne("Coda")
                        .HasForeignKey("XamarinSync.Database.Entities.DocCoda", "IdDoc")
                        .HasPrincipalKey("XamarinSync.Database.Entities.DocTesta", "IdDoc")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.DocRiga", b =>
                {
                    b.HasOne("XamarinSync.Database.Entities.Articolo", "Articolo")
                        .WithMany()
                        .HasForeignKey("CodArt")
                        .HasPrincipalKey("CodArt");

                    b.HasOne("XamarinSync.Database.Entities.DocTesta")
                        .WithMany("Righe")
                        .HasForeignKey("IdDoc")
                        .HasPrincipalKey("IdDoc")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.DocTesta", b =>
                {
                    b.HasOne("XamarinSync.Database.Entities.Cliente", "Cliente")
                        .WithMany()
                        .HasForeignKey("IdAnagrafica")
                        .HasPrincipalKey("IdAnagrafica")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
