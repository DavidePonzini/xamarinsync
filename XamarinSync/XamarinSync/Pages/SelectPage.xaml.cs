﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectPage : MasterDetailPage
	{
		private FileImageSource _syncIcon;

		public SelectPage()
		{
			InitializeComponent();

			var toolbarItem = new ToolbarItem("Synchronize", "sync.png", Synchronize);
			_syncIcon = toolbarItem.Icon;

			ToolbarItems.Add(toolbarItem);

			MasterPage.ListView.ItemSelected += ListView_ItemSelected;
			
		}

		private async void Synchronize()
		{
			await Navigation.PushModalAsync(new SynchronizationPage());
		}

		private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as SelectPageMenuItem;
			if (item == null)
				return;

			var page = (Page) Activator.CreateInstance(item.TargetType, item.Entities, item.EntityType);
			page.Title = item.Title;

			Detail = new NavigationPage(page);	
	
			IsPresented = false;

			MasterPage.ListView.SelectedItem = null;
		}
	}
}