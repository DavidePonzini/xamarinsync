﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinSync.Database.Entities
{
	public class DocTesta : Entity
	{
		public int IdDoc { get; set; }
		public virtual List<DocRiga> Righe { get; set; } = new List<DocRiga>();
		public virtual DocCoda Coda { get; set; } = new DocCoda();

		public int IdAnagrafica { get; set; }
		public virtual Cliente Cliente { get; set; }

		public int NumeroDocumento { get; set; }
		public DateTime DataDocumento { get; set; }
		public float TotDocumento { get; set; }
		

		public override object[] Keys => new object[] {IdDoc};
	}
}
