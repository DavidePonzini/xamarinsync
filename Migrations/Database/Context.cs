﻿using Microsoft.EntityFrameworkCore;
using XamarinSync.Database.Entities;

namespace XamarinSync.Database
{
	public class Context : DbContext
	{
		private readonly string _databasePath;

		public DbSet<LastSynchronization> LastSynchronizations { get; set; }
		public DbSet<Area> Aree { get; set; }
		public DbSet<CondizioniPagamentoRighe> Righe { get; set; }
		public DbSet<CondizioniPagamentoTeste> Teste { get; set; }
		public DbSet<Operai> Operai { get; set; }

		// Ordini
		public DbSet<DocTesta> DocTeste { get; set; }
		public DbSet<DocRiga> DocRighe { get; set; }
		public DbSet<DocCoda> DocCode { get; set; }
		public DbSet<Cliente> Clienti { get; set; }
		public DbSet<Articolo> Articoli { get; set; }





		public Context() { }
		public Context(string databasePath)
		{
			_databasePath = databasePath;

		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite($"Filename={_databasePath}");
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<CondizioniPagamentoRighe>()
				.HasOne(r => r.Testa)
				.WithMany(t => t.Righe)
				.HasForeignKey(r => r.CodPagamento)
				.HasPrincipalKey(t => t.CodPagamento);




			modelBuilder.Entity<DocTesta>()
				.HasMany(t => t.Righe)
				.WithOne()
				.HasForeignKey(r => r.IdDoc)
				.HasPrincipalKey(t => t.IdDoc);

			modelBuilder.Entity<DocTesta>()
				.HasOne(t => t.Coda)
				.WithOne()
				.HasForeignKey<DocCoda>(c => c.IdDoc)
				.HasPrincipalKey<DocTesta>(t => t.IdDoc);

			modelBuilder.Entity<DocRiga>()
				.HasOne(r => r.Articolo)
				.WithMany()
				.HasForeignKey(r => r.CodArt)
				.HasPrincipalKey(a => a.CodArt);

			modelBuilder.Entity<DocTesta>()
				.HasOne(t => t.Cliente)
				.WithMany()
				.HasForeignKey(t => t.IdAnagrafica)
				.HasPrincipalKey(c => c.IdAnagrafica);
		}
	}
}
