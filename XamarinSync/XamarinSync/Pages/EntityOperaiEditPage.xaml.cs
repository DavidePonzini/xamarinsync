﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityOperaiEditPage : ContentPage
	{
		private readonly Operai _operaio;
		private readonly bool _bIsNewEntity;

		public EntityOperaiEditPage(Operai operaio = null)
		{
			InitializeComponent();

			EntityEditUtil.Init(operaio, out _operaio, out _bIsNewEntity);

			EntryDescrizione.Text = _operaio.Descrizione;
			EntryCellulare.Text = _operaio.Cellulare;
			EntryCodCentro.Text = _operaio.CodCentro;
			EntryCostoMan.Text = _operaio.CostoMan.ToString("F");
			EntryMail.Text = _operaio.Mail;
		}


		private async void ButtonSave_OnClicked(object sender, EventArgs e)
		{
			_operaio.Descrizione = EntryDescrizione.Text;
			_operaio.Cellulare = EntryCellulare.Text;
			_operaio.CodCentro = EntryCodCentro.Text;
			_operaio.CostoMan = float.Parse(EntryCostoMan.Text);
			_operaio.Mail = EntryMail.Text;


			if (_bIsNewEntity)
			{
				var context = await App.GetDatabaseContext();

				await EntityEditUtil.SaveInsert(_operaio, context.Operai);
			}
			else
			{
				await EntityEditUtil.SaveEdit(_operaio);
			}
			await Navigation.PopAsync();
		}

	}
}