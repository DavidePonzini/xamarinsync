﻿namespace XamarinSync.Database.Entities
{
	public class Articolo : Entity
	{
		public string CodArt { get; set; }
		public string Descrizione { get; set; }

		public override object[] Keys => new object[] {CodArt};

		public override string ToString() => Descrizione;
	}
}