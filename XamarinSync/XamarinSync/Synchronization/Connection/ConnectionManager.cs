﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using XamarinSync.Database;
using XamarinSync.Database.Entities;
using XamarinSync.Synchronization.Serialization;

namespace XamarinSync.Synchronization.Connection
{
	public static class ConnectionManager
	{
		private const string TokenUrl = "http://10.0.0.210/AppServer/api/token/1/FreeBikes";
		private const string GetUrl = "http://10.0.0.210/AppServer/odata/1/FreeBikes";
		private const string PostUrl = "http://10.0.0.210/AppServer/apicard/1/FreeBikes";
		private const string PutUrl = "http://10.0.0.210/AppServer/apicard/1/FreeBikes";

		public static string Username { get; private set; }
		public static string Password { get; private set; }
		private static string _authToken;
		public static bool IsTokenSet => _authToken != null;

		private static readonly HttpClient Client = new HttpClient();

		public static async Task LoginAsync(string username, string password)
		{
			Username = username;
			Password = password;

			await GetTokenAsync();
		}

		private static async Task<OdataResponse<T>> DownloadAsync_Internal<T>(string url)
			where T : new()
		{
			await SetClientAuthentication();

			var response = await Client.GetAsync(url);

			if (!response.IsSuccessStatusCode)
			{
				var c = await response.Content.ReadAsStringAsync();
				throw new ConnectionFailedException(response);
			}

			var content = response.Content;

			var json = await content.ReadAsStringAsync();

			var odata = JsonConvert.DeserializeObject<OdataResponse<T>>(json);

			return odata;
		}

		[Obsolete]
		public static async void TEST()
		{
			await DownloadAsync_Internal<Area>("http://10.0.0.210/AppServer/apicard/1/FreeBikes/OrdiniCliente/");
		}

		private static async Task SetClientAuthentication()
		{
			if (_authToken == null)
				await GetTokenAsync();

			Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
		}

		public static async Task<OdataResponse<T>> DownloadAsync<T>(string table, DateTime lastSync, int howMany, int from)
			where T : new()
		{
			var url = GetUrl + '/' + table +
			          BuildQuery(
				          "$top=" + howMany,
				          "$skip=" + from,
				          "$filter=UltimaModifica gt " + FormatDate(lastSync)
			          );

			return await DownloadAsync_Internal<T>(url);
		}

		private static string BuildQuery(params string[] args)
		{
			var sb = new StringBuilder("?");
			var enumerator = args.GetEnumerator();

			if (!enumerator.MoveNext())
				return "";
			sb.Append(enumerator.Current);

			while (enumerator.MoveNext())
				sb.Append("&").Append(enumerator.Current);

			return sb.ToString();
		}

		private static string FormatDate(DateTime date)
		{
			return
				date.Year.ToString("D4") + '-' +
				date.Month.ToString("D2") + '-' +
				date.Day.ToString("D2") +
				'T' +
				date.Hour.ToString("D2") + ':' +
				date.Minute.ToString("D2") + ':' +
				date.Second.ToString("D2") + '.' +
				date.Millisecond +
				"%2b01:00";
		}

		public static async Task<OdataResponse<T>> DownloadAsync<T>(string table, int howMany, int from)
			where T : new()
		{
			var url = GetUrl + '/' + table +
				BuildQuery("$top=" + howMany, "$skip=" + from);

			return await DownloadAsync_Internal<T>(url);
		}

		private static async Task GetTokenAsync()
		{
			var authentication = Convert.ToBase64String(Encoding.ASCII.GetBytes(Username + ":" + Password));

			Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Default", authentication);
			
			var response = await Client.GetAsync(TokenUrl);

			if (!response.IsSuccessStatusCode)
				throw new ConnectionFailedException(response);

			var content = response.Content;
			var res = await content.ReadAsStringAsync();
			_authToken = JsonConvert.DeserializeObject<string>(res);
		}


		public static async Task<EntityKeys[]> InsertAsync(string feed, Entity entity)
		{
			var url = PostUrl + '/' + feed;

			await SetClientAuthentication();

			var content = PrepareContent(entity);

			var response = await Client.PostAsync(url, content);

			if (!response.IsSuccessStatusCode)
				throw new ConnectionFailedException(response);

			var json = await response.Content.ReadAsStringAsync();
			var insertResponse = JsonConvert.DeserializeObject<InsertResponse>(json);

			return insertResponse.Keys;
		}
		public static async Task UpdateAsync(string feed, Entity entity)
		{
			var url = PutUrl + '/' + feed;

			await SetClientAuthentication();

			var content = PrepareContent(entity);

			var response = await Client.PutAsync(url, content);
				
			if (!response.IsSuccessStatusCode)
				throw new ConnectionFailedException(response);
		}

		private static StringContent PrepareContent(Entity entity)
		{
			var serializerSettings = new JsonSerializerSettings {ContractResolver = new JsonContractResolver()};
			var json = JsonConvert.SerializeObject(entity, serializerSettings);

			var content = new StringContent(json, Encoding.UTF8, "application/json");
			return content;
		}
	}
}
