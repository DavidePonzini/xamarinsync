﻿using System;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Synchronization;
using XamarinSync.Synchronization.Conflicts;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SynchronizationPage : ContentPage
	{
		public SynchronizationPage()
		{
			InitializeComponent();
			Synchronize();
		}

		private async void Synchronize()
		{
			var token = new CancellationTokenSource();
			RotationAnim(token.Token);
			try
			{
				await SynchronizationManager.Synchronize();
			}
			catch (UnresolvedConflictsException)
			{
				SetButtonConflict();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
			finally
			{
				token.Cancel();
			}
		}

		private void SetButtonConflict()
		{
			var conflicts = ConflictManager.Conflicts.Count;

			ButtonConflict.Text = conflicts + " Conflicts";

			ButtonConflict.BorderColor = conflicts > 0 ? Color.Red : BackgroundColor;
		}		

		private async void RotationAnim(CancellationToken cancellationToken)
		{
			while (!cancellationToken.IsCancellationRequested)
			{
				await Image.RelRotateTo(-360, 2000, Easing.Linear);
			}
			await Image.FadeTo(0.0);
		}

		private async void ButtonConflict_OnClicked(object sender, EventArgs e)
		{
			try
			{
				await Navigation.PushModalAsync(new NavigationPage(new ConflictPage()));
			} catch(Exception ex)
			{ }
		}

		private async void ButtonClose_OnClicked(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			SetButtonConflict();
		}
	}
}