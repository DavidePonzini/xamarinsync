﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using XamarinSync.Database;

namespace XamarinSync.Migrations
{
    [DbContext(typeof(Context))]
    [Migration("20170914073851_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("XamarinSync.Database.Entities.Area", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CodArea");

                    b.Property<string>("Descrizione");

                    b.Property<int>("IdMemoNote");

                    b.Property<int>("State");

                    b.HasKey("Guid");

                    b.ToTable("Aree");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.CondizioniPagamentoRighe", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CodPagamento");

                    b.Property<bool>("FineMese");

                    b.Property<int>("GgRata");

                    b.Property<int>("IdTipologiaPagamento");

                    b.Property<int>("NrRata");

                    b.Property<float>("PercRata");

                    b.Property<int>("State");

                    b.HasKey("Guid");

                    b.HasIndex("CodPagamento");

                    b.ToTable("Righe");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.CondizioniPagamentoTeste", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AnnoCommerciale");

                    b.Property<bool>("ChiusuraAutomaticaScadenze");

                    b.Property<string>("CodContoIncassi");

                    b.Property<int>("CodPagamento");

                    b.Property<string>("Descrizione");

                    b.Property<int>("NrRate");

                    b.Property<bool>("PagamentoAnticipato");

                    b.Property<float>("PercMaggiorazione");

                    b.Property<float>("PercSconto");

                    b.Property<bool>("RateCostanti");

                    b.Property<float>("SpeseIncasso");

                    b.Property<int>("State");

                    b.Property<int>("TipoGestioneIva");

                    b.Property<string>("TipoPagamentoPA");

                    b.HasKey("Guid");

                    b.ToTable("Teste");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.Operai", b =>
                {
                    b.Property<int>("Guid")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("AttivitaPerAltri");

                    b.Property<bool>("AttivitaTicketPerAltri");

                    b.Property<bool>("BloccaGruppo");

                    b.Property<bool>("BloccaOperatore");

                    b.Property<string>("Cellulare");

                    b.Property<string>("CodCentro");

                    b.Property<string>("CodContoANAManodoperaDefault");

                    b.Property<string>("CodContoANAManodperaDefaultCdc");

                    b.Property<string>("CodOperaio");

                    b.Property<string>("CodRuolo");

                    b.Property<float>("CostNotturnoFestivo");

                    b.Property<float>("CostoFestivo");

                    b.Property<float>("CostoMan");

                    b.Property<float>("CostoNotturno");

                    b.Property<float>("CostoStraordinario");

                    b.Property<string>("DepositoDefault");

                    b.Property<string>("Descrizione");

                    b.Property<bool>("Disattivo");

                    b.Property<string>("GruppoCrm");

                    b.Property<int>("IdCalendario");

                    b.Property<bool>("ImpegniAttivi");

                    b.Property<int>("Interno");

                    b.Property<string>("Mail");

                    b.Property<bool>("OperatoreCalendario");

                    b.Property<bool>("OperatoreTSQL");

                    b.Property<bool>("OutlookAttivo");

                    b.Property<bool>("Predefinito");

                    b.Property<float>("RicavoMan");

                    b.Property<int>("ScheduleID");

                    b.Property<int>("State");

                    b.Property<int>("TempoMan");

                    b.Property<string>("UtenteLogin");

                    b.Property<string>("codEtichetta");

                    b.HasKey("Guid");

                    b.ToTable("Operai");
                });

            modelBuilder.Entity("XamarinSync.Database.LastSynchronization", b =>
                {
                    b.Property<string>("Feed")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Time");

                    b.HasKey("Feed");

                    b.ToTable("LastSynchronizations");
                });

            modelBuilder.Entity("XamarinSync.Database.Entities.CondizioniPagamentoRighe", b =>
                {
                    b.HasOne("XamarinSync.Database.Entities.CondizioniPagamentoTeste", "Testa")
                        .WithMany("Righe")
                        .HasForeignKey("CodPagamento")
                        .HasPrincipalKey("CodPagamento")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
