﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSync.Database.Entities;

namespace XamarinSync.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntityDocTestaEditPage : ContentPage
	{
		private Cliente _cliente;
		private ObservableCollection<DocRiga> _righe;

		private readonly DocTesta _testa;
		private readonly bool _bIsNewEntity;

		public EntityDocTestaEditPage(DocTesta testa = null)
		{
			InitializeComponent();

			EntityEditUtil.Init(testa, out _testa, out _bIsNewEntity);
			_cliente = _testa.Cliente;
			_righe = new ObservableCollection<DocRiga>(_testa.Righe);

			EntryPercentualeSconto.Keyboard = Keyboard.Numeric;
			EntryNumeroDocumento.Keyboard = Keyboard.Numeric;
			EntryTotale.Keyboard = Keyboard.Numeric;

			LabelCliente.Text = _cliente == null ? "Not Selected" : _testa.Cliente.RagioneSociale;
			EntryNumeroDocumento.Text = _testa.NumeroDocumento.ToString();
			EntryTotale.Text = _testa.TotDocumento.ToString("F");
			EntryPercentualeSconto.Text = _testa.Coda.PercSconto.ToString("F");
			DataDocumento.Date = _testa.DataDocumento;
			ListViewArticoli.ItemsSource = _righe;
		}


		private async void ButtonSave_OnClicked(object sender, EventArgs e)
		{
			_testa.Righe = _righe.ToList();
			_testa.Cliente = _cliente;
			_testa.DataDocumento = DataDocumento.Date;
			_testa.TotDocumento = float.Parse(EntryTotale.Text);
			_testa.NumeroDocumento = int.Parse(EntryNumeroDocumento.Text);
			_testa.Coda.PercSconto = float.Parse(EntryPercentualeSconto.Text);


			if (_bIsNewEntity)
			{
				var context = await App.GetDatabaseContext();

				await EntityEditUtil.SaveInsert(_testa, context.DocTeste);
			}
			else
			{
				await EntityEditUtil.SaveEdit(_testa);
			}

			await Navigation.PopAsync();
		}

		private async void ButtonChangeCliente_OnClicked(object sender, EventArgs e)
		{
			var context = await App.GetDatabaseContext();

			var page = new EntitySelectPage {ItemSource = context.Clienti};
			page.ItemSelected += OnClienteSelected;

			await Navigation.PushAsync(page);
		}

		private void OnClienteSelected(object o, EventArgs args)
		{
			var item = (Cliente) o;
			_cliente = item;
			LabelCliente.Text = item.RagioneSociale;
		}

		private async void ButtonAddRow_OnClicked(object sender, EventArgs e)
		{
			var context = await App.GetDatabaseContext();

			var page = new EntitySelectPage {ItemSource = context.Articoli};
			page.ItemSelected += OnArticoloSelected;

			await Navigation.PushAsync(page);
		}

		private void OnArticoloSelected(object o, EventArgs e)
		{
			var item = (Articolo) o;
			var riga = new DocRiga {Articolo = item};

			_righe.Add(riga);
		}

		private async void ListViewArticoli_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem == null)
				return;

			var riga = (DocRiga) e.SelectedItem;
			var listView = (ListView) sender;

			var action = await DisplayActionSheet(riga.Articolo.Descrizione, "Close", null, "Change Quantity", "Delete");
			switch (action)
			{
			case "Change Quantity":
				var page = new EntityDocRigaEditQuantityPage(riga);
				page.OnSave += (o, args) =>
				{
					// Hacky way of firing OnPropertyChanged,
					//	proper way would be to implement INotifyPropertyChanged
					_righe.Remove(riga);
					_righe.Add(riga);
				};

				await Navigation.PushModalAsync(page);
				break;
			case "Delete":
				_righe.Remove(riga);
				break;
			}

			listView.SelectedItem = null;
		}
	}
}