﻿using System;
using System.IO;
using XamarinSync;
using XamarinSync.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(FileHelper))]
namespace XamarinSync.Droid
{
	public class FileHelper : IFileHelper
	{
		public string GetFilePath(string filename)
		{
			var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

			return Path.Combine(path, filename);
		}
	}
}