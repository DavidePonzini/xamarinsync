using System;

namespace XamarinSync.Synchronization.Serialization
{
	public class JsonSerializeNameAttribute : Attribute
	{
		public string Name { get; }

		public JsonSerializeNameAttribute(string name)
		{
			Name = name;
		}
	}
}