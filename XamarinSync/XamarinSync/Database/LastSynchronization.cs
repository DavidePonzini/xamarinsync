﻿using System;
using System.ComponentModel.DataAnnotations;

namespace XamarinSync.Database
{
	public class LastSynchronization
	{
		[Key]
		public string Feed { get; set; }
		public DateTime Time { get; set; }
	}
}
