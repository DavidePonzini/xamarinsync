﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace XamarinSync.Migrations
{
    public partial class Ordini : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Articoli",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CodArt = table.Column<string>(nullable: false),
                    Descrizione = table.Column<string>(nullable: true),
                    State = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articoli", x => x.Guid);
                    table.UniqueConstraint("AK_Articoli_CodArt", x => x.CodArt);
                });

            migrationBuilder.CreateTable(
                name: "Clienti",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IdAnagrafica = table.Column<int>(nullable: false),
                    RagioneSociale = table.Column<string>(nullable: true),
                    State = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clienti", x => x.Guid);
                    table.UniqueConstraint("AK_Clienti_IdAnagrafica", x => x.IdAnagrafica);
                });

            migrationBuilder.CreateTable(
                name: "DocTeste",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DataDocumento = table.Column<DateTime>(nullable: false),
                    IdAnagrafica = table.Column<int>(nullable: false),
                    IdDoc = table.Column<int>(nullable: false),
                    NumeroDocumento = table.Column<int>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    TotDocumento = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocTeste", x => x.Guid);
                    table.UniqueConstraint("AK_DocTeste_IdDoc", x => x.IdDoc);
                    table.ForeignKey(
                        name: "FK_DocTeste_Clienti_IdAnagrafica",
                        column: x => x.IdAnagrafica,
                        principalTable: "Clienti",
                        principalColumn: "IdAnagrafica",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocCode",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IdDoc = table.Column<int>(nullable: false),
                    PercSconto = table.Column<float>(nullable: false),
                    State = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocCode", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_DocCode_DocTeste_IdDoc",
                        column: x => x.IdDoc,
                        principalTable: "DocTeste",
                        principalColumn: "IdDoc",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocRighe",
                columns: table => new
                {
                    Guid = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CodArt = table.Column<string>(nullable: true),
                    IdDoc = table.Column<int>(nullable: false),
                    IdRiga = table.Column<int>(nullable: false),
                    Qta = table.Column<float>(nullable: false),
                    State = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocRighe", x => x.Guid);
                    table.ForeignKey(
                        name: "FK_DocRighe_Articoli_CodArt",
                        column: x => x.CodArt,
                        principalTable: "Articoli",
                        principalColumn: "CodArt",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocRighe_DocTeste_IdDoc",
                        column: x => x.IdDoc,
                        principalTable: "DocTeste",
                        principalColumn: "IdDoc",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocCode_IdDoc",
                table: "DocCode",
                column: "IdDoc",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocRighe_CodArt",
                table: "DocRighe",
                column: "CodArt");

            migrationBuilder.CreateIndex(
                name: "IX_DocRighe_IdDoc",
                table: "DocRighe",
                column: "IdDoc");

            migrationBuilder.CreateIndex(
                name: "IX_DocTeste_IdAnagrafica",
                table: "DocTeste",
                column: "IdAnagrafica");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocCode");

            migrationBuilder.DropTable(
                name: "DocRighe");

            migrationBuilder.DropTable(
                name: "Articoli");

            migrationBuilder.DropTable(
                name: "DocTeste");

            migrationBuilder.DropTable(
                name: "Clienti");
        }
    }
}
